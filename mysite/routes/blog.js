var express = require('express');
var router = express.Router();
const Blogs=require('../models/blog')

router.get('/', function(req, res, next) {
    res.render("blog/index",{data:"index"});
  });

  router.get('/add', function(req, res, next) {
    res.render("blog/addform",{data:"add"});
  });

  router.post('/add', function(req, res, next) {
     data=new Blogs({
       id:req.body.id,
       name:req.body.name,
       tel:req.body.tel
     })
     Blogs.createBlog(data,function(err,callback){
       if(err) console.log(err);
       res.redirect("/")
     })
  });

module.exports = router;